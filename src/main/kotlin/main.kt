package gzip

import java.io.*
import java.util.zip.GZIPOutputStream
import java.io.ByteArrayOutputStream




class Gzip {
    companion object {

        @JvmStatic

        fun main(args: Array<String>) {

            val inPath = "in.txt"
            val gzipPath = "gzip.gz"
            val outPath = "out.txt"
            compress(inPath, gzipPath)
            parse(gzipPath, outPath)
        }

        fun compress(inPath: String, gzipPath: String){

            val fis = FileInputStream(inPath)
            val fos = FileOutputStream(gzipPath)
            val gzipOS = GZIPOutputStream(fos)
            val buffer = ByteArray(1024)
            var len: Int

            while (true) {
                len = fis.read(buffer)
                if (len == -1) break
                gzipOS.write(buffer, 0, len)
            }

            gzipOS.close()
            fos.close()
            fis.close()
        }

        fun parse(gzipPath: String, outPath: String) {

            val fos = FileOutputStream(outPath)
            val dos = DataOutputStream(fos)

            val fis = FileInputStream(gzipPath)
            val dis = DataInputStream(fis)

            var fileType = ""
            var FHCRC = 0
            var FEXTRA = 0
            var FNAME = 0
            var fileName = ""
            var FCOMMENT = 0
            var comment = ""
            var OS = ""

            var temp = dis.readUnsignedByte()

            if (temp == 0x1F) {
                temp = dis.readUnsignedByte()
                if (temp == 0x8B) {
                    fileType = "gzip"
                    temp = dis.readUnsignedByte()
                    FHCRC = temp and 0b00000010
                    FEXTRA = temp and 0b00000100
                    FNAME = temp and 0b00001000
                    FCOMMENT = temp and 0b00010000
                }
                else{
                    println("не gzip")
                    return
                }
            }
            else {
                println("точно не gzip")
                return
            }

            dis.readDouble() // пропуск 4х бесполезных байт

            OS = when(dis.readUnsignedByte()){
                0 -> "FAT"
                1 -> "Amiga"
                2 -> "VMS"
                3 -> "UNIX"
                4 -> "VM/CMS"
                5 -> "Atari TOS"
                6 -> "HPFS"
                7 -> "Mac"
                8 -> "Z-system"
                9 -> "CP/M"
                10 -> "TOPS-20"
                11 -> "NTFS"
                12 -> "QDOS"
                13 -> "RISKOS"
                else -> "unknown"
            }
            println("OS: $OS")

            if (FEXTRA != 0){
                var xlen = dis.readUnsignedByte()
                while(xlen > 0){
                    temp = dis.readUnsignedByte()
                }
            }

            if (FNAME != 0) {
                fileName = readNullTerminatedString(dis)
                println("File name: $fileName")
            }

            if (FCOMMENT != 0) {
                comment = readNullTerminatedString(dis)
            }

            decompress(dis,dos)

        }

        fun readNullTerminatedString(dis: DataInputStream): String{
            var string = ""
            var baos = ByteArrayOutputStream()

            while (true){
                val temp = dis.read()
                baos.write(temp)
                if (temp == 0x00) break
            }
            return baos.toByteArray().toString(Charsets.US_ASCII)
        }

        fun decompress(dis: DataInputStream, dos: DataOutputStream){

            var isLastBlock = true
            var typeBlock = ""

            do{
                var temp = dis.readUnsignedByte()

                if ((temp and 0x01) == 1){
                    isLastBlock = false
                }

                when ((temp and 0x06)){
                    0 -> noCompression(temp, dis)
                    2 -> fixedHuffmanCodes(temp, dis)
                    4 -> dynamicHuffmanCodes(temp, dis)
                    6 -> errorBlock(temp, dis)
                }

            }while (isLastBlock)
        }


        private fun errorBlock(temp: Int, dis: DataInputStream) {

        }

        private fun dynamicHuffmanCodes(temp: Int, dis: DataInputStream) {

        }

        private fun fixedHuffmanCodes(temp: Int, dis: DataInputStream) {

        }

        private fun noCompression(temp: Int, dis: DataInputStream){

        }
    }
}