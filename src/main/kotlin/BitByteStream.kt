import java.io.InputStream
import jdk.nashorn.tools.ShellFunctions.input



class  BitByteStream constructor(private val input: InputStream,
                                 private var currentByte: Int = 0,
                                 private var remainingBits: Int = 0) {

    fun getBitPosition(): Int {
        if (remainingBits < 0 || remainingBits > 7)
            throw IllegalStateException()
        return (8 - remainingBits) % 8
    }

    fun readByte(): Int {
        currentByte = 0
        remainingBits = 0
        return input.read()
    }

    fun readBit(): Int{

        if (currentByte === -1)
            return -1
        if (remainingBits === 0) {
            currentByte = input.read()

            if (currentByte === -1)
                return -1
            remainingBits = 8
        }
        if (remainingBits <= 0)
            throw AssertionError()
        remainingBits--
        return currentByte.ushr(7 - remainingBits) and 1
    }

}